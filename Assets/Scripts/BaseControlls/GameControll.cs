﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameControll : MonoBehaviour
{
    [SerializeField] private Car GMC9500;

    private void Start()
    {
        CameraControll.Singleton.SetCameraToCar(GMC9500);
    }
}

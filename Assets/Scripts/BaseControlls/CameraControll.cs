﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControll : MonoBehaviour
{
    public Camera GameCamera;

    public static CameraControll Singleton { get; private set; }

    private void Awake()
    {
        if (Singleton)
        {
            Destroy(gameObject);
            return;
        }
        else
        {
            Singleton = this;
        }
    }

    public void SetCameraToCar(Car car)
    {
        car.SetCamera();
    }

    private void OnDestroy()
    {
        if (Singleton == this)
            Singleton = null;
    }
}

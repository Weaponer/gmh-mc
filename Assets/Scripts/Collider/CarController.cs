/******************************************
 * CarController
 *  
 * This class was created by:
 * 
 * Nic Tolentino.
 * rotatingcube@gmail.com
 * 
 * I take no liability for it's use and is provided as is.
 * 
 * The classes are based off the original code developed by Unity Technologies.
 * 
 * You are free to use, modify or distribute these classes however you wish, 
 * I only ask that you make mention of their use in your project credits.
*/
using UnityEngine;
using System.Collections;

public class CarController : MonoBehaviour
{
    public Transform FrontRight;
    public Transform FrontLeft;
    public Transform BackRight;
    public Transform BackLeft;

    private WheelColliderSource FrontRightWheel;
    private WheelColliderSource FrontLeftWheel;
    private WheelColliderSource BackRightWheel;
    private WheelColliderSource BackLeftWheel;

    public void Start()
    {
        FrontRightWheel = FrontRight.gameObject.GetComponent<WheelColliderSource>();
        FrontLeftWheel = FrontLeft.gameObject.GetComponent<WheelColliderSource>();
        BackRightWheel = BackRight.gameObject.GetComponent<WheelColliderSource>();
        BackLeftWheel = BackLeft.gameObject.GetComponent<WheelColliderSource>();
    }

    public void FixedUpdate()
    {
        //Apply the accelerator pedal
        FrontRightWheel.MotorTorque = Input.GetAxis("Vertical") * 300.0f;
        FrontLeftWheel.MotorTorque = Input.GetAxis("Vertical") * 300.0f;

        //Turn the steering wheel
        FrontRightWheel.SteerAngle = Input.GetAxis("Horizontal") * 45;
        FrontLeftWheel.SteerAngle = Input.GetAxis("Horizontal") * 45;

        //Apply the hand brake
        if (Input.GetKey(KeyCode.Space))
        {
            BackRightWheel.BrakeTorque = 0.8f;
            BackLeftWheel.BrakeTorque = 0.8f;
        }
        else //Remove handbrake
        {
            BackRightWheel.BrakeTorque = 0;
            BackLeftWheel.BrakeTorque = 0;
        }
    }
}

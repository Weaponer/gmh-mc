﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForntSuspension : AnimPartTruck
{
    [SerializeField] private WheelColliderSource colliderLeftFront;

    [SerializeField] private WheelColliderSource colliderRightFront;

    [SerializeField] private Transform leftSuspPoint;

    [SerializeField] private Transform rightSuspPoint;

    [SerializeField] private Transform leftWheel;

    [SerializeField] private Transform rightWheel;


    Vector3 localCenter;

    Vector3 normalDirect;

    float dist;
    private void Start()
    {
        dist = Vector3.Distance(leftSuspPoint.position, rightSuspPoint.position);
        /*   normalDirect = car.transform.InverseTransformPoint(rightSuspPoint.position) - car.transform.InverseTransformPoint(leftSuspPoint.position);
           normalDirect = normalDirect.normalized;
           localCenter = car.transform.InverseTransformPoint(leftSuspPoint.position) + normalDirect * (dist / 2);*/

    }



    public override void Tick()
    {
        Vector3 direct = colliderRightFront.GetPosition() - colliderLeftFront.GetPosition();
        direct = direct.normalized;

        float ldist = Vector3.Distance(colliderRightFront.GetPosition(), colliderLeftFront.GetPosition());
        Vector3 pos = colliderLeftFront.GetPosition() + direct * (ldist / 2f);
        pos = car.transform.InverseTransformPoint(pos);
        pos.x = 0;
        pos = car.transform.TransformPoint(pos);

        leftSuspPoint.position = pos - direct * (dist / 2f);
        rightSuspPoint.position = pos + direct * (dist / 2f);
        Quaternion rot = Quaternion.LookRotation(Vector3.Cross(-car.transform.forward, -direct), -direct);
        leftSuspPoint.rotation = rot;
        rightSuspPoint.rotation = rot;

        leftWheel.position = leftSuspPoint.position;
        leftWheel.rotation = Quaternion.LookRotation(direct, car.transform.up);
        leftWheel.localRotation *= Quaternion.Euler(0, colliderLeftFront.GetSteering(), 0);
        leftWheel.localRotation *= Quaternion.Euler(0, 0, colliderLeftFront.GetAngleRotate());

        rightWheel.position = rightSuspPoint.position;
        rightWheel.rotation = Quaternion.LookRotation(-direct, car.transform.up);
        rightWheel.localRotation *= Quaternion.Euler(0, colliderRightFront.GetSteering(), 0);
        rightWheel.localRotation *= Quaternion.Euler(0, 0, -colliderRightFront.GetAngleRotate());
    }
}

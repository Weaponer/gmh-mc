﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cardan : AnimPartTruck
{
    [SerializeField] private Transform mountOne;

    [SerializeField] private Transform mountTwo;

    [SerializeField] private Transform cardan;

    [SerializeField] private WheelColliderSource[] wheels;

    float dist;

    float angle;

    Quaternion cardanDefault;

    Vector3 defaultDirect;

    private void Start()
    {
        dist = Vector3.Distance(mountOne.position, mountTwo.position);
        cardanDefault = Quaternion.Inverse(car.transform.rotation) * cardan.rotation;
        defaultDirect = Quaternion.Inverse(car.transform.rotation) * cardan.up;
    }

    public override void Tick()
    {
        cardan.localScale = new Vector3(1, Vector3.Distance(mountOne.position, mountTwo.position) / dist, 1);

        cardan.position = mountOne.position;


        cardan.rotation = car.transform.rotation * cardanDefault;

        Vector3 norm = (mountOne.position - mountTwo.position).normalized;
        cardan.rotation = Quaternion.LookRotation(norm, car.transform.up);


        cardan.rotation *= Quaternion.Euler(90, 0, 0);


        mountTwo.rotation = Quaternion.LookRotation(car.transform.up, mountTwo.up);
        mountOne.rotation = Quaternion.LookRotation(car.transform.up, mountOne.up);

        // cardan.rotation = Quaternion.LookRotation(cardan.forward, car.transform.up);

        float add = 0;
        for (int i = 0; i < wheels.Length; i++)
        {
            add += wheels[i].GetAngleVelocity() * Time.deltaTime;
        }

        angle += add / wheels.Length;

        mountTwo.Rotate(Vector3.up, angle, Space.Self);
        mountOne.Rotate(Vector3.up, angle, Space.Self);
        cardan.Rotate(Vector3.up, angle, Space.Self);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Car : MonoBehaviour
{
    [SerializeField] protected Vector3 sizeCar;

    [SerializeField] protected Vector3 offsetCameraCar;

    [SerializeField] protected Vector3 positionInSalon;

    [SerializeField] protected CameraCarControll cameraCar;

    [SerializeField] protected Rigidbody rigidbody;


    [SerializeField] protected List<AnimPartTruck> animPartTrucks = new List<AnimPartTruck>();

    protected bool isCameraControll;

    private void Awake()
    {
        cameraCar.Init(sizeCar, offsetCameraCar, positionInSalon, this);
    }

    public void SetCamera()
    {
        isCameraControll = true;
        cameraCar.Enable();
        CallSetCamera();
    }

    protected virtual void CallSetCamera()
    {
    }

    private void Update()
    {
        for (int i = 0; i < animPartTrucks.Count; i++)
        {
            animPartTrucks[i].Tick();
        }
    }

    public void GetCamera()
    {
        isCameraControll = false;
        cameraCar.Disable();
        CallGetCamera();
    }

    public Vector3 GetVelocity()
    {
        return rigidbody.velocity;
    }

    protected virtual void CallGetCamera()
    {
    }
}

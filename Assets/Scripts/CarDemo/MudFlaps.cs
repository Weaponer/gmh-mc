﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MudFlaps : AnimPartTruck
{
    [SerializeField] private Transform boneLeft;
    [SerializeField] private Transform boneRight;

    float oldZ = 0;

    float vel = 0;
    public override void Tick()
    {
        Vector3 localVel = car.transform.InverseTransformDirection(car.GetVelocity());
        float rez = (oldZ - localVel.z);

        vel = vel - rez;


      //  boneLeft.localEulerAngles = new Vector3(0, vel * 35f, 0);
     //   boneRight.localEulerAngles = new Vector3(0, vel * 35f, 0);
        vel = vel + (0f - vel * 0.1f);
        oldZ = localVel.z;
    }
}

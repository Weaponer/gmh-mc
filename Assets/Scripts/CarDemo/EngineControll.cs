﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EngineControll : MonoBehaviour
{
    public float RPM { get { return rpm; } }

    public int Gear { get { return gear; } }

    public float Torque { get; private set; }

    public float Brake { get; private set; }

    public bool IsActive { get; private set; }

    public float Exhaust { get; set; }

    [SerializeField] private float minRPM;
    [SerializeField] private float maxRPM;
    [SerializeField] private int startGearIndex;

    [SerializeField] private float[] gearRate;

    [SerializeField] private EngineSound engineSound;

    [SerializeField] private WheelColliderSource[] wheels;



    Coroutine starter;

    float force;

    private float rpm;

    int gear;

    private void Start()
    {
        gear = startGearIndex;
    }

    private void Update()
    {
        UpdateInput();
        UpdatePhys();
        for (int i = 0; i < wheels.Length; i++)
        {
            wheels[i].MotorTorque = Torque * 3.5f;
            wheels[i].MorotRotates = RPM * 3f / Mathf.Abs(Torque * 0.01f + 1);
            wheels[i].BrakeTorque = Brake;
        }
        engineSound.UpdateAudio();
    }

    private void UpdateInput()
    {
        if (starter == null && Input.GetKeyDown(KeyCode.B))
        {
            if (!IsActive)
            {
                StartEngine();
            }
            else
            {
                StopEngine();
            }
        }
        if (IsActive)
        {
            if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.S))
            {
                force = 20;
            }
            else
            {
                force = 0;
            }
        }
        if (Input.GetKeyDown(KeyCode.X))
        {
            NextGear();
        }
        else if (Input.GetKeyDown(KeyCode.Z))
        {
            BackGear();
        }

        if (Input.GetKey(KeyCode.Space))
        {
            Brake = 0.7f;
        }
        else
        {
            Brake = 0;
        }
    }

    private void UpdatePhys()
    {
        if (IsActive)
        {
            rpm += (force - 10) * (1 - Mathf.Abs(gearRate[gear]));
            Torque = rpm * Mathf.Abs((1 - gearRate[gear]) * Mathf.Ceil(Mathf.Abs(gearRate[gear])));
            Torque *= Mathf.Sign(gearRate[gear]);
            rpm = Mathf.Clamp(rpm, minRPM, maxRPM);
            engineSound.SetRPM(rpm);
            Exhaust += rpm / (maxRPM * 45f);
           
        }
        else
        {
            Torque = 0;
            rpm = 0;
        }
    }

    private void NextGear()
    {
        if (gear < gearRate.Length - 1)
        {
            gear++;
            engineSound.ChangeGear();
            rpm -= 2500 * Mathf.Abs(gearRate[gear]);
        }
    }

    private void BackGear()
    {
        if (gear > 0)
        {
            gear--;
            engineSound.ChangeGear();
            rpm -= 2500 * Mathf.Abs(gearRate[gear]);
        }
    }

    private void StopEngine()
    {
        engineSound.EngineStop();
        IsActive = false;
    }

    private void StartEngine()
    {
        starter = StartCoroutine(Starter());
    }

    IEnumerator Starter()
    {
        engineSound.EngineStart(1.4f);
        yield return new WaitForSeconds(1.4f);
        IsActive = true;
        starter = null;
    }
}

[System.Serializable]
public class EngineSound
{
    [Header("Engine box")]
    [SerializeField] private AudioSource starter;
    [Range(0, 2f)]
    [SerializeField] private float starterEnd;

    [Range(0, 3500f)]
    [SerializeField] private float maxPRM;

    [Range(0, 1500f)]
    [SerializeField] private float minPRM;


    [SerializeField] private AudioSource engineIdle;
    [SerializeField] private AudioSource engineHight;

    [Header("Gear box")]
    [SerializeField] private AudioSource gearChange;


    float starterToIdleTime = 0;
    float starterToIdle = 0;
    bool engineStart;

    float rpm;
    public void EngineStart(float starterTime)
    {
        if (starterTime < starterEnd)
        {
            starterToIdleTime = starterTime;
        }
        else
        {
            starterToIdleTime = starterEnd;
        }
        starter.Play();
        engineIdle.Play();
        engineHight.Play();
        starter.volume = 1;
        engineIdle.volume = 0;
        engineHight.volume = 0;
        engineStart = true;
    }

    public void EngineStop()
    {
        engineIdle.Stop();
        engineHight.Stop();
    }

    public void UpdateAudio()
    {
        if (engineStart)
        {
            if (starter.time >= starterToIdleTime)
            {
                starterToIdle = (starter.clip.length - starter.time) / (starter.clip.length - starterToIdleTime);


                starter.volume = starterToIdle;
                engineIdle.volume = (1f - starterToIdle) * 0.1f;

            }
            else
            {
                starterToIdle = 0;
            }
            if (starter.isPlaying == false)
            {
                engineStart = false;
            }
        }
        if (engineHight.isPlaying && !engineStart)
        {
            float mult =  Mathf.Clamp( (rpm  - minPRM) / maxPRM,0.1f,0.95f );

            engineIdle.volume = (1f - mult) * mult;
            engineHight.volume = (mult) * mult;
        }
    }

    public void SetRPM(float rpm)
    {
        this.rpm = rpm;
    }

    public void ChangeGear()
    {
        gearChange.Play();
    }


}
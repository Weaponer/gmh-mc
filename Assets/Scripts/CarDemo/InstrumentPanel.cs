﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstrumentPanel : AnimPartTruck
{
    [SerializeField] protected Transform arrow;
    [SerializeField] protected Vector2 agnleRange;
    [SerializeField] protected Vector2 valueRange;

    private Quaternion localRotate;
    private void Start()
    {
        localRotate = arrow.localRotation;
    }

    protected void SetValue(float value)
    {
        value = Mathf.Clamp(value, valueRange.x, valueRange.y);
        value /= valueRange.y;
        float ang = agnleRange.x + (Mathf.Abs(agnleRange.x) + Mathf.Abs(agnleRange.y)) * value;
        arrow.localRotation = localRotate * Quaternion.Euler(ang, 0, 0);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteeringWheel : MonoBehaviour
{
    [Min(0)]
    [SerializeField] private float angle;

    [Min(0)]
    [SerializeField] private float speed;

    [SerializeField] WheelColliderSource leftFront;
    [SerializeField] WheelColliderSource rightFront;
    public float Steering { get { return steering; } }

    float steering;
    private void Update()
    {
        int a = 0, b = 0;
        if (Input.GetKey(KeyCode.D))
        {
            b = 1;
        }
        if (Input.GetKey(KeyCode.A))
        {
            a = 1;
        }

        if (b == 1 || a == 1)
        {
            steering += speed * b * Time.deltaTime;
            steering -= speed * a * Time.deltaTime;
        }
        else if (Mathf.Abs(0 - steering) > 2f)
        {
            steering += speed * 0.1f * Mathf.Sign(0 -steering) * Time.deltaTime;
        }

        steering = Mathf.Clamp(steering, -angle, angle);

        leftFront.SteerAngle = steering;
        rightFront.SteerAngle = steering;
    }
}

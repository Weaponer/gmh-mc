﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimSteeringwheel : AnimPartTruck
{
    [SerializeField] private SteeringWheel steeringWheel;

    [SerializeField] private Transform obj;
    public override void Tick()
    {
        obj.rotation = Quaternion.LookRotation(car.transform.right, obj.up);
        obj.RotateAround(obj.up, steeringWheel.Steering * 0.13f);
    }
}

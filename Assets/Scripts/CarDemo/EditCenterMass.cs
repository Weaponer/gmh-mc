﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EditCenterMass : MonoBehaviour
{
    [SerializeField] private Vector3 centerMass;
    void Start()
    {
        GetComponent<Rigidbody>().centerOfMass = centerMass;
    }

}

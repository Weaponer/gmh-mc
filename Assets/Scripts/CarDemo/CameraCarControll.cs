﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CameraCarMode
{
    Outside,
    InSalon
}

public class CameraCarControll : MonoBehaviour
{
    Vector3 sizeCar;

    Vector3 offsetCameraCar;

    Vector3 positionInSalon;

    Car baseCar;


    float midDist;

    float scaleDist;

    CameraCarMode mode;

    Vector3 angle;

    bool isMove;

    bool active;

    Vector3 saveDirectToCar;

    public void Init(Vector3 sizeCar, Vector3 offsetCameraCar, Vector3 positionInSalon, Car baseCar)
    {
        this.sizeCar = sizeCar;
        this.positionInSalon = positionInSalon;
        this.offsetCameraCar = offsetCameraCar;
        this.baseCar = baseCar;

        midDist = Mathf.Max(sizeCar.x, sizeCar.y, sizeCar.z);
    }


    public void Enable()
    {
        active = true;
    }

    private void Update()
    {
        if (active)
        {
            UpdateMode();

            if (Input.GetMouseButton(1))
            {
                isMove = true;
                MoveCamera();
            }
            else
            {
                isMove = false;
                if (mode == CameraCarMode.Outside)
                    RotateToVelocity();
            }
            if (mode == CameraCarMode.Outside)
                ScaleDist();
        }
    }


    private void UpdateMode()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            if (mode == CameraCarMode.InSalon)
            {
                mode = CameraCarMode.Outside;
            }
            else
            {
                mode = CameraCarMode.InSalon;
            }
        }
    }

    private void MoveCamera()
    {
        angle.x += -Input.GetAxis("Mouse Y") * 5f;
        angle.y += Input.GetAxis("Mouse X") * 5f;
        if (mode == CameraCarMode.Outside)
        {

            angle.x = Mathf.Clamp(angle.x, 0, 90);
        }
        else
        {
            angle.x = Mathf.Clamp(angle.x, -75, 10);
            angle.y = Mathf.Clamp(angle.y, -70, 70);
        }

        if (mode == CameraCarMode.Outside)
        {
            Vector3 globalDirect = new Vector3(Mathf.Sin(angle.y * Mathf.Deg2Rad), 0, Mathf.Cos(angle.y * Mathf.Deg2Rad));
            Vector3 forwardTruck = Vector3.Cross(Vector3.up, baseCar.transform.right);
            saveDirectToCar = Quaternion.Inverse(Quaternion.LookRotation(forwardTruck, Vector3.up)) * globalDirect;
        }
    }

    private void ScaleDist()
    {

        scaleDist += -Input.GetAxis("Mouse ScrollWheel") * 3f;
        scaleDist = Mathf.Clamp(scaleDist, 0, 3f);
    }

    private void RotateToVelocity()
    {
        float vel = baseCar.GetVelocity().magnitude;
        vel = Mathf.Clamp(vel, 0, 10);
        Quaternion thisAngleCam = Quaternion.Euler(0, angle.y, 0);

        Quaternion trukAngle = Quaternion.LookRotation(Vector3.Cross(Vector3.up, baseCar.transform.right), Vector3.up);

        Quaternion truckAngleCam = Quaternion.identity;
        if (saveDirectToCar != Vector3.zero)
           truckAngleCam  = Quaternion.LookRotation(trukAngle * saveDirectToCar, Vector3.up);

        Quaternion newAngle = Quaternion.Lerp(thisAngleCam, truckAngleCam, vel / 10f / 70f);

        angle.y = newAngle.eulerAngles.y;
    }

    private void LateUpdate()
    {
        if (active)
        {
            Transform camera = CameraControll.Singleton.GameCamera.transform;
            if (mode == CameraCarMode.Outside)
            {
                camera.rotation = Quaternion.Euler(angle);
                camera.position = baseCar.transform.position + offsetCameraCar;
                camera.position += -camera.forward * (midDist + scaleDist);
            }
            else
            {
                camera.position = baseCar.transform.TransformPoint(positionInSalon);
                Quaternion newAngle = baseCar.transform.rotation * Quaternion.Euler(angle);
                camera.rotation = newAngle;
            }
        }
    }

    public void Disable()
    {
        active = false;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RearSuspension : AnimPartTruck
{

    [SerializeField] private Transform baseSusp;

    [SerializeField] private Transform baseSpring;

    [SerializeField] private Transform frontAxle;

    [SerializeField] private Transform rearAxle;



    [SerializeField] private Transform wheelLF;

    [SerializeField] private Transform wheelRF;

    [SerializeField] private Transform wheelLR;

    [SerializeField] private Transform wheelRR;

    [SerializeField] private WheelColliderSource pointWheelLF;

    [SerializeField] private WheelColliderSource pointWheelRF;

    [SerializeField] private WheelColliderSource pointWheelLR;

    [SerializeField] private WheelColliderSource pointWheelRR;

    float distWheel;

    private void Start()
    {
        distWheel = Vector3.Distance(wheelLF.position, wheelRF.position);
    }

    public override void Tick()
    {
        Transform carT = car.transform;


        //fronAxle
        Vector3 center = pointWheelLF.GetPosition() + pointWheelRF.GetPosition();
        Vector3 normal = (pointWheelRF.GetPosition() - pointWheelLF.GetPosition()).normalized;
        center /= 2f;

        center = carT.InverseTransformPoint(center);
        center.x = 0;
        center = carT.TransformPoint(center);
        frontAxle.position = center;
        frontAxle.rotation = Quaternion.LookRotation(normal, carT.up);

        wheelLF.position = pointWheelLF.GetPosition();
        wheelLF.rotation = Quaternion.LookRotation(normal, car.transform.up);
        wheelLF.localRotation *= Quaternion.Euler(0, 0, pointWheelLF.GetAngleRotate());

        wheelRF.position = pointWheelRF.GetPosition();
        wheelRF.rotation = Quaternion.LookRotation(-normal, car.transform.up);
        wheelRF.localRotation *= Quaternion.Euler(0, 0, -pointWheelRF.GetAngleRotate());


        //RearAxle
        center = pointWheelLR.GetPosition() + pointWheelRR.GetPosition();
        normal = (pointWheelRR.GetPosition() - pointWheelLR.GetPosition()).normalized;
        center /= 2f;

        center = carT.InverseTransformPoint(center);
        center.x = 0;
        center = carT.TransformPoint(center);

        rearAxle.position = center;
        rearAxle.rotation = Quaternion.LookRotation(normal, carT.up);

        wheelLR.position = pointWheelLR.GetPosition();
        wheelLR.rotation = Quaternion.LookRotation(normal, car.transform.up);
        wheelLR.localRotation *= Quaternion.Euler(0, 0, pointWheelLR.GetAngleRotate());

        wheelRR.position = pointWheelRR.GetPosition();
        wheelRR.rotation = Quaternion.LookRotation(-normal, car.transform.up);
        wheelRR.localRotation *= Quaternion.Euler(0, 0, -pointWheelRR.GetAngleRotate());

        //BaseSpring
        float yBase = carT.InverseTransformPoint(baseSusp.position).y;
        float yAxleF = carT.InverseTransformPoint(frontAxle.position).y;
        float yAxleR = carT.InverseTransformPoint(rearAxle.position).y;
        Vector3 springPos = carT.InverseTransformPoint(baseSpring.position);
        springPos.y = (yBase + yAxleF + yAxleR) / 3f;
        springPos = carT.TransformPoint(springPos);
        baseSpring.position = springPos;
    }
}

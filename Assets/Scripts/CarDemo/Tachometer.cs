﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tachometer : InstrumentPanel
{
    public override void Tick()
    {
        SetValue(((Truck)car).GetRPM());
    }
}

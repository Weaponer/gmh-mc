﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Speedometer : InstrumentPanel
{
    public override void Tick()
    {
        SetValue(((Truck)car).GetVelocity().magnitude * 3.6f);
    }
}

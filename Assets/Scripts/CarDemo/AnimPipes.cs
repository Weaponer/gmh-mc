﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimPipes : AnimPartTruck
{
    [SerializeField] private Transform leftPipes;

    [SerializeField] private Transform rightPipes;

    [SerializeField] private Transform leftLid;

    [SerializeField] private Transform rightLid;

    [SerializeField] private ParticleSystem leftEffect;

    [SerializeField] private ParticleSystem rightEffect;

    float time;

    bool releaseExtruct;

    float extruct;

    float angle;

    public override void Tick()
    {
        Truck truck = (Truck)car;

        if (truck.IsActiveEngine())
        {
            time += Time.deltaTime * 100;

            Vector3 v = leftPipes.localEulerAngles;

            v.x = Mathf.Sin(time) * 0.6f;
            v.y = Mathf.Cos(time) * 0.6f;
            leftPipes.localEulerAngles = v;

            v = rightPipes.localEulerAngles;

            v.x = Mathf.Sin(time) * 0.6f;
            v.y = -Mathf.Cos(time) * 0.6f;
            rightPipes.localEulerAngles = v;


            if (truck.GetEngine().Exhaust > 1f && !releaseExtruct)
            {
                releaseExtruct = true;
                extruct = truck.GetEngine().Exhaust;
                leftEffect.Play();
                rightEffect.Play();
            }

            if (releaseExtruct)
            {
                truck.GetEngine().Exhaust = 0;
                extruct -= 0.005f;
                if (extruct <= 0)
                {
                    releaseExtruct = false;
                    leftEffect.Stop();
                    rightEffect.Stop();
                }
            }


        }
        else
        {
            if (leftEffect.isPlaying)
            {
                leftEffect.Stop();
                rightEffect.Stop();
            }
        }

        if (extruct > 0.4f)
        {
            float a = Mathf.Sign(-50 - angle) * 90 * Time.deltaTime;
            angle += a;

            leftLid.Rotate(new Vector3(0, a, 0), Space.Self);
            rightLid.Rotate(new Vector3(0, -a, 0), Space.Self);
        }
        else if (extruct <= 0.4f)
        {
            float a = Mathf.Sign(0 - angle) * 90 * Time.deltaTime;
            angle += a;

            leftLid.Rotate(new Vector3(0, a, 0), Space.Self);
            rightLid.Rotate(new Vector3(0, -a, 0), Space.Self);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Truck : Car
{
    [SerializeField] private EngineControll engine;

    public float GetRPM()
    {
        return engine.RPM;
    }

    public bool IsActiveEngine()
    {
        return engine.IsActive;
    }

    public EngineControll GetEngine()
    {
        return engine;
    }
}
